# Algorithms & Graphics with ThreeJS

This repo contains all the necessary files for the course.

## Getting Started
1. Clone or download this repo.
2. Launch static server: i.e `serve session_01`

### THREE autocompletion in VS Code with Typescript
`npm install typings --global`

Then from project folder:
```
typings init
typings search three
typings install dt~three --global --save
```

Source: [http://shrekshao.github.io/2016/06/20/vscode-01/](http://shrekshao.github.io/2016/06/20/vscode-01/)