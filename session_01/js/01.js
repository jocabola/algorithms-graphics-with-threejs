/*
// Assume we have WebGL enabled --> Discuss fallback
1. Create Renderer
2. Setup Renderer (size, background, AA, transparent) + Embedd
3. Create Scene
4. Create Orthographic Camera
5. Create Box Geometry
6. Create Basic Material (color + wireframe option too)
7. Create Mesh
8. Position, rotate, etc
9. Add mesh to Scene
10. Render once
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setPixelRatio(window.devicePixelRatio||1);

document.querySelector(".container").appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
camera.position.z = 500;
scene.add(camera);

var mat = new THREE.MeshBasicMaterial({color: 0xff0000, wireframe: true});

var bgeo = new THREE.BoxGeometry(200,200,200);
var box = new THREE.Mesh(bgeo,mat);
scene.add(box);

var sgeo = new THREE.SphereGeometry(100, 32, 16);
var sphere = new THREE.Mesh(sgeo,mat);
scene.add(sphere);
sphere.visible = false;

var igeo = new THREE.IcosahedronGeometry(100, 2);
var ico = new THREE.Mesh(igeo,mat);
scene.add(ico);
ico.visible = false;

renderer.render(scene,camera);