/*
 LINE LOOP VS LINE VS LINE SEGMENTS
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setPixelRatio(window.devicePixelRatio||1);

document.querySelector(".container").appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
camera.position.z = 500;
scene.add(camera);

var geo = new THREE.Geometry();
geo.vertices.push(new THREE.Vector3(-50, 50, 0)); // x y z
geo.vertices.push(new THREE.Vector3(50, 50, 0)); // x y z
geo.vertices.push(new THREE.Vector3(50, -50, 0)); // x y z
geo.vertices.push(new THREE.Vector3(-50, -50, 0)); // x y z

var cols = [];

for (let i=0; i<4; i++) {
    var col = new THREE.Color(0xffffff * Math.random());
    geo.colors.push(col);
}

var mat = new THREE.LineBasicMaterial({vertexColors: THREE.VertexColors});

// --- LINE LOOP ------------------------------------------------
var line = new THREE.LineLoop(
    geo,
    mat
)
line.position.x = -140;
scene.add(line);

// --- LINE ------------------------------------------------
var line2 = new THREE.Line(
    geo,
    mat
);
scene.add(line2);

var line3 = new THREE.LineSegments(
    geo,
    mat
);
line3.position.x = 140;
scene.add(line3);

var animate = function() {
    requestAnimationFrame(animate);
    renderer.render(scene,camera);
}

animate();