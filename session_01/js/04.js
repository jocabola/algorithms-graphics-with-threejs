/*
Animation
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setClearColor(0xffffff, 0);
renderer.setPixelRatio(window.devicePixelRatio||1);

document.querySelector(".container").appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
camera.position.z = 500;
scene.add(camera);

var mat = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors, wireframe: false});

var igeo = new THREE.IcosahedronGeometry(150, 1);
var nv = igeo.vertices.length;
var oPos = [];

// inject vertex colors + store original positions
for (var i=0;i<nv;i++) {
    igeo.colors.push(new THREE.Color(Math.random()*0xffffff));
    oPos.push(new THREE.Vector3().copy(igeo.vertices[i]));
}

for(var f of igeo.faces) {
    f.vertexColors.push(igeo.colors[f.a]);
    f.vertexColors.push(igeo.colors[f.b]);
    f.vertexColors.push(igeo.colors[f.c]);
}

var ico = new THREE.Mesh(igeo,mat);
scene.add(ico);

var map = document.createElement("canvas");
map.width = map.height = 512;
var ctx = map.getContext('2d');
ctx.fillStyle = "#fff";
ctx.beginPath();
ctx.arc(256, 256, 200, 0, 2*Math.PI);
ctx.fill();

var pts = new THREE.Points(
    igeo,
    new THREE.PointsMaterial({size: 16, vertexColors: THREE.VertexColors, map: new THREE.CanvasTexture(map), transparent: true})
);

pts.visible = false;
scene.add(pts);

// --- ANIMATION & RENDER STUFF ----------------------------------------------------

var clock = new THREE.Clock(true);

var settings = {
    rXT: 20,
    rZT: 16,
    amp: 10,
    moveVertices: false,
    showGui: true
};

function resetVertices () {
    if (!settings.moveVertices) {
        for (let i=0; i<nv; i++) {
            pos.array[i*3] = pArrayCopy[i*3];
            pos.array[i*3+1] = pArrayCopy[i*3+1];
        }
        pos.needsUpdate = true;
    }
}

if ( settings.showGui ) {
    var gui = new dat.GUI({width: 320});
    gui.close();
    gui.add( settings, "rXT", 1, 60 ).name("Rev Duration X (s)");
    gui.add( settings, "rZT", 1, 60 ).name("Rev Duration Z (s)");
    gui.add( settings, "moveVertices" ).name("Move Vertices").onChange(resetVertices);
    gui.add( settings, "amp", 0, 40 ).name("Vertex deformation");
    gui.add( mat, "wireframe" ).name("Wireframe");
    gui.add( pts, "visible" ).name("Show Points");
}

var transformIcosahedron = function () {
    var t = clock.getElapsedTime();

    if ( settings.moveVertices ) {
        for (var i=0; i<nv; i++) {
            igeo.vertices[i].x = oPos[i].x + settings.amp * Math.sin(oPos[i].x+t);
            igeo.vertices[i].y = oPos[i].y + settings.amp * Math.sin(oPos[i].y+t);
        }

        igeo.verticesNeedUpdate = true;
    }

    ico.rotation.y = 2 * Math.PI * (t % settings.rXT)/settings.rXT;
    ico.rotation.z = 2 * Math.PI * (t % settings.rZT)/settings.rZT;

    pts.rotation.copy(ico.rotation);
}

var animate = function() {
    requestAnimationFrame(animate);
    transformIcosahedron();
    renderer.render(scene,camera);
}

animate();