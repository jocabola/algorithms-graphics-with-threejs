/*
Interaction
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setClearColor(0xffffff, 0);
renderer.setPixelRatio(window.devicePixelRatio||1);

var container = document.querySelector(".container");

container.appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
//var camera = new THREE.PerspectiveCamera(75,1,.001,10000);
camera.position.z = 500;
scene.add(camera);

var mat = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors, wireframe: false});

var igeo = new THREE.IcosahedronGeometry(150, 1);
var nv = igeo.vertices.length;
var oPos = [];

// inject vertex colors + store original positions
for (var i=0;i<nv;i++) {
    igeo.colors.push(new THREE.Color(Math.random()*0xffffff));
    oPos.push(new THREE.Vector3().copy(igeo.vertices[i]));
}

for(var f of igeo.faces) {
    f.vertexColors.push(igeo.colors[f.a]);
    f.vertexColors.push(igeo.colors[f.b]);
    f.vertexColors.push(igeo.colors[f.c]);
}

var ico = new THREE.Mesh(igeo,mat);
scene.add(ico);

var map = document.createElement("canvas");
map.width = map.height = 512;
var ctx = map.getContext('2d');
ctx.fillStyle = "#fff";
ctx.beginPath();
ctx.arc(256, 256, 200, 0, 2*Math.PI);
ctx.fill();

pgeo = new THREE.Geometry();
pgeo.vertices.push(new THREE.Vector3());

var pts = new THREE.Points(
    pgeo,
    new THREE.PointsMaterial({size: 20, color: 0xff0000, map: new THREE.CanvasTexture(map), transparent: true, depthTest: false})
);

pts.visible = false;

scene.add(pts);

// --- INTERACTION STUFF -----------------------------------------------------------

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2(-1,-1);
var intersected = null;

container.addEventListener("mousemove", function (event) {
    let rect = container.getBoundingClientRect();
    let mx = event.clientX-rect.x;
    let my = event.clientY-rect.y;
    //console.log(mx,my);
    mouse.set(-1+2*mx/512, 1-2*my/512);
    // console.log(mouse.x, mouse.y);
}, false);

// --- ANIMATION & RENDER STUFF ----------------------------------------------------

var clock = new THREE.Clock(true);

var rXT = 20;
var rZT = 16;

function transformIcosahedron () {
    let t = clock.getElapsedTime();
    ico.rotation.y = 2 * Math.PI * (t % rXT )/rXT;
    ico.rotation.z = 2 * Math.PI * (t % rZT)/rZT;
}

function interact () {
    raycaster.setFromCamera(mouse, camera);
    let intersects = raycaster.intersectObject(ico);
    pts.visible = intersects.length > 0;
    if ( pts.visible ) {
        intersected = intersects[0];
        pts.position.copy( intersects[0].point );
    }
}

var animate = function() {
    requestAnimationFrame(animate);
    transformIcosahedron();
    interact();
    renderer.render(scene,camera);
}

animate();