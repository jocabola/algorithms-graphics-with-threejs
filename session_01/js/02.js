/*
1. Create Renderer
2. Setup Renderer (size, background, AA, transparent) + Embedd
3. Create Scene
4. Create Orthographic Camera
5. Create Buffer Geometry with 3 vertices (positions + color)
6. Create Mesh (triangle), Points, Line Loop
7. Position
8. Add meshes to Scene
9. Render once
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setPixelRatio(window.devicePixelRatio||1);

document.querySelector(".container").appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
camera.position.z = 500;
scene.add(camera);

 var geo = new THREE.Geometry();
 geo.vertices.push(
     new THREE.Vector3(-180, 180, 0), // x y z
     new THREE.Vector3(180, 0, 0), // x y z
     new THREE.Vector3(-180, -180, 0) // x y z
 );

 for (let i=0; i<3; i++) {
     var col = new THREE.Color(0xffffff * Math.random());
     geo.colors.push(col);
 }



// --- POINTS ----------------------------------------------------
var points = new THREE.Points(
    geo,
    new THREE.PointsMaterial({size: 10, vertexColors: THREE.VertexColors})
);

scene.add(points);

// --- TRIANGLES -------------------------------------------------
var geo2 = geo.clone();
var face = new THREE.Face3(2,1,0);
face.vertexColors.push(geo2.colors[2], geo2.colors[1], geo2.colors[0]);

geo2.faces.push(face);
var tri = new THREE.Mesh(
    geo2,
    new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors})
)

scene.add(tri);

// --- LINE LOOP ------------------------------------------------
var geo3 = geo.clone();
var line = new THREE.LineLoop(
    geo3,
    new THREE.LineBasicMaterial({vertexColors: THREE.VertexColors})
)

scene.add(line);
line.visible = false;

var animate = function() {
    requestAnimationFrame(animate);
    renderer.render(scene,camera);
}

animate();