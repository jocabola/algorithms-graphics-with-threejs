/*
Interaction
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setClearColor(0xffffff, 0);
renderer.setPixelRatio(window.devicePixelRatio||1);

var container = document.querySelector(".container");

container.appendChild(renderer.domElement);

var scene = new THREE.Scene();
// var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 100000); // left, right, top, bottom
var camera = new THREE.PerspectiveCamera(75,1,.001,1000);
camera.position.z = 320;
scene.add(camera);

var mat = new THREE.MeshBasicMaterial({wireframe: true, color: 0x999999});

var igeo = new THREE.SphereGeometry( 150, 16, 8 );

var seed = new THREE.Mesh(igeo,mat);
scene.add(seed);
seed.visible = false;

// --- PLACE PLANES ALONG  VERTICES -------------------------------------------

var box = new THREE.BoxGeometry(5,5,5);
var mat2 = new THREE.MeshNormalMaterial();

var boxes = [];

for ( let i=0; i<igeo.vertices.length; i++ ) {
    var p = igeo.vertices[i];
    var mesh = new THREE.Mesh(box, mat2);
    mesh.position.copy(p);
    mesh.oPosition = p.clone();
    boxes.push(mesh);
    scene.add(mesh);
}

// --- INTERACTION STUFF -----------------------------------------------------------

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2(-1,-1);
var intersected = null, selected=null;

var controls = new THREE.OrbitControls(camera, container);
controls.autoRotateSpeed = .3;
controls.enableDamping = true;
controls.minPolarAngle = Math.PI/4;
controls.maxPolarAngle = 3*Math.PI/4;
controls.enableZoom = false;
controls.enablePan = false;

var blocked = false;

container.addEventListener("mousemove", function (event) {
    let rect = container.getBoundingClientRect();
    let mx = event.clientX-rect.x;
    let my = event.clientY-rect.y;
    //console.log(mx,my);
    mouse.set(-1+2*mx/512, 1-2*my/512);
    // console.log(mouse.x, mouse.y);
}, false);

// --- SETTINGS & GUI --------------------------------------------------------------

var settings = {
    showGui: true
};

if ( settings.showGui ) {
    var gui = new dat.GUI({width: 320});
    var f1 = gui.addFolder("Controls");
    f1.add( controls, "autoRotate" ).name("Auto Rotate Cam");
    f1.add( controls, "autoRotateSpeed", .1, 16 ).name("Auto Rotate Speed");
    f1.add( controls, "dampingFactor", .01, .5 ).name("Damping Factor");
    f1.add( controls, "enableZoom" ).name("Zoom");
    f1.add( controls, "enablePan" ).name("Pan");
    // f1.open();
    gui.add( seed, "visible" ).name("Show Seed");
}

// --- ANIMATION & RENDER STUFF ----------------------------------------------------

var clock = new THREE.Clock(true);

function hover (obj) {
    if ( obj == null ) return;
    obj.scale.set(2,2,2);
}

function release (obj) {
    if ( obj == null ) return;
    obj.scale.set(1,1,1);
}

function interact () {
    controls.update();

    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects(boxes);

    if ( intersects.length ) {
        container.style.cursor = 'pointer';
        if ( intersects[0].object != intersected ) {
            release(intersected);
            intersected = intersects[0].object;
            hover(intersected);
        }
    }
    else {
        release(intersected);
        intersected = null;
        container.style.cursor = 'default';
    }
}

var animate = function() {
    requestAnimationFrame(animate);
    let t = clock.getElapsedTime();
    for ( let mesh of boxes ) {
        mesh.rotation.y += Math.sin( mesh.position.x + t ) * .04;
        mesh.position.y = mesh.oPosition.y + Math.sin( .1 * Math.PI * mesh.oPosition.x/100 + t ) * 5;
    }
    interact();
    renderer.render(scene,camera);
}

animate();