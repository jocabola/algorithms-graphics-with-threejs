varying vec2 vUv;

uniform int mode;
uniform sampler2D map;

void main() {
    if ( mode == 1 ) gl_FragColor = vec4(vUv,0.0,1.0); // rgba
    else {
        gl_FragColor = texture2D(map, vUv);
    }
}