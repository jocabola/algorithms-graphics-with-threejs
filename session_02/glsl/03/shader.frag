varying vec3 vColor;

void main() {
    /*
     * Possible vec4 combinations:
        vec4(1.0); // 1,1,1,1
        vec4(vec3 v, 1.0) // v.xyz, 1
        vec4(vec2 v, 1.0, 1.0) // v.xy, 1, 1
     */
    gl_FragColor = vec4(vColor,1.0); // rgba
}