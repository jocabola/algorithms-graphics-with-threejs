precision lowp float;

attribute vec3 position;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

/*
    Atrribute sizes
    --------------------------------------
    Attribute: 1 num per vertex --> float
    Attribute: 2 nums per vertex --> vec2
    Attribute: 3 nums per vertex --> vec3
    Attribute: 4 nums per vertex --> vec4

    ****************************************
    
    Uniform types
    ---------------------------------------
    uniform: THREE.Vector2 = vec2
    uniform: THREE.Vector3 = vec3
    uniform: THREE.Color(rgb) = vec3
    uniform: THREE.Vector4 = vec4
    uniform: THREE.Matrix3 = mat3
    uniform: THREE.Matrix4 = mat4
 */

void main() {
    // Apply transform of mesh (mesh.position + mesh.rotation + mes.scale)
    vec4 mvPos = modelViewMatrix * vec4(position, 1.0);
    
    // Apply transform of view (relative to camera used: Perspective, Orthographic, cam.position, cam.rotation, cam.lookAt....)
    gl_Position = projectionMatrix * mvPos;
}