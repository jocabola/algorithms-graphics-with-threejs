varying vec2 vUv;

uniform int mode;
uniform float time;
uniform sampler2D map;

const float PI = 3.1415926535897932384626433832795;
const float TWO_PI = 2.0 * PI;

void main() {
    if ( mode == 0 ) {
        float d = distance(vUv, vec2(.5));
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 1 ) {
        float d = step(.25, vUv.x);
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 2 ) {
        float d = smoothstep(.25, .5, vUv.x);
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 3 ) {
        float d = sin(vUv.x*TWO_PI);
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 4 ) {
        float d = abs(sin(vUv.x*TWO_PI));
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 5 ) {
        float d = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI));
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 6 ) {
        float d = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI+time));
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if ( mode == 7 ) {
        float r = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI+time));
        float g = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI*4.0+time*2.0+ 1.4));
        float b = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI*8.0+time*.5+ .77));
        gl_FragColor = vec4(r,g,b,1.0);
    }
    else if ( mode == 8 ) {
        float r = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI+time)*cos(vUv.y*TWO_PI+time*1.6));
        float g = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI*4.0+time*2.0+ 1.4 * vUv.y)*cos(vUv.y*TWO_PI*8.0+1.4+time));
        float b = smoothstep(-1.0,1.0,sin(vUv.x*TWO_PI*8.0+time*.5+ .77)*cos(vUv.y*TWO_PI*2.0+time*2.6 + 1.4));
        gl_FragColor = vec4(r,g,b,1.0);
    }
    else if ( mode == 9 ) {
        float d = smoothstep(.5, 1., 1.0-distance(vUv, vec2(.5)));
        gl_FragColor = texture2D(map, vUv) * d;
    }
    else if ( mode == 10 ) {
        float x = smoothstep(-1.0,1.0,sin(time));
        float y = smoothstep(-1.0,1.0,cos(time));
        float d = smoothstep(.85, 1., 1.0-distance(vUv, vec2(x,y)));
        gl_FragColor = texture2D(map, vUv) * d;
    }
    else if ( mode == 11 ) {
        float x = smoothstep(-1.0,1.0,sin(time+vUv.x));
        float y = smoothstep(-1.0,1.0,cos(time+vUv.y));
        float d = smoothstep(.85, 1., 1.0-distance(vUv, vec2(x,y)));
        gl_FragColor = vec4(texture2D(map, vUv).rgb * d, 1.0);
        // To-Do: Alter x,y
    }
    else {
        gl_FragColor = texture2D(map, vUv);
    }
}