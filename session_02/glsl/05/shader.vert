varying vec2 vUv;

void main() {
    vUv = uv;

    // Apply transform of mesh (mesh.position + mesh.rotation + mes.scale)
    vec4 mvPos = modelViewMatrix * vec4(position, 1.0);
    
    // Apply transform of view (relative to camera used: Perspective, Orthographic, cam.position, cam.rotation, cam.lookAt....)
    gl_Position = projectionMatrix * mvPos;
}