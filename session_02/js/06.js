/*
Lights
*/

var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setClearColor(0xffffff, 0);
renderer.setPixelRatio(window.devicePixelRatio||1);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;//THREE.BasicShadowMap;

var container = document.querySelector(".container");

container.appendChild(renderer.domElement);

var scene = new THREE.Scene();
// var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 100000); // left, right, top, bottom
var camera = new THREE.PerspectiveCamera(75,1,.001,10000);
camera.position.z = 350;
camera.position.y = 250;
camera.lookAt(new THREE.Vector3());
scene.add(camera);

// --- ADD OBJECTS TO SCENE -------------------------------------------

var tl = new THREE.TextureLoader();
var texture = tl.load("img/floor.jpg");
texture.repeat.set(4,4);
texture.wrapS = THREE.RepeatWrapping;
texture.wrapT = THREE.RepeatWrapping;

var floor = new THREE.Mesh(
    new THREE.PlaneGeometry(1024, 1024),
    new THREE.MeshStandardMaterial({color: 0xcccccc, map: texture, side: THREE.DoubleSide, roughness: .9, metalness: .6})
);

floor.rotation.x = -Math.PI/2;
floor.receiveShadow = true;

scene.add(floor);



// --- SETUP LIGHTS ----------------------------------------------------------------
var ambient = new THREE.AmbientLight(0xeeeeee);
scene.add(ambient);

var sun = new THREE.DirectionalLight(0xFAF7D7, 1);
sun.position.set(500, 300, 0);
sun.target.position.set(0,0,0);
sun.castShadow = true;

sun.shadow.camera.near = 1;
sun.shadow.camera.far = 1024;
sun.shadow.camera.right = 512;
sun.shadow.camera.left = - 512;
sun.shadow.camera.top	= 512;
sun.shadow.camera.bottom = - 512;
sun.shadow.mapSize.width = 1024;
sun.shadow.mapSize.height = 1024;

scene.add(sun);

var shadowHelper = new THREE.CameraHelper( sun.shadow.camera );
// scene.add(shadowHelper);

// --- GLTF ------------------------------------------------------------------------

var loader = new THREE.GLTFLoader();

loader.load(
	// resource URL
	'/gltf/dog/model.gltf',
	// called when the resource is loaded
	function ( gltf ) {

        console.log(gltf);
        var mesh = gltf.scene.children[0];
        console.log(mesh);
        scene.add(mesh);
        mesh.geometry.center();
        mesh.scale.set(200,200,200);
        mesh.position.y = 42;
        mesh.castShadow = true;
        mesh.rotation.y = Math.PI;

		gltf.animations; // Array<THREE.AnimationClip>
		gltf.scene; // THREE.Scene
		gltf.scenes; // Array<THREE.Scene>
		gltf.cameras; // Array<THREE.Camera>
		gltf.asset; // Object

	},
	// called while loading is progressing
	function ( xhr ) {

		//console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

	},
	// called when loading has errors
	function ( error ) {

		console.log( 'An error happened' );

	}
);

// --- INTERACTION STUFF -----------------------------------------------------------

var controls = new THREE.OrbitControls(camera, container);
// controls.autoRotate = true;
controls.autoRotateSpeed = .2;
controls.enableDamping = true;
controls.minPolarAngle = Math.PI/4;
controls.maxPolarAngle = 3*Math.PI/4;
// controls.enableZoom = false;
// controls.enablePan = false;

// --- SETTINGS & GUI --------------------------------------------------------------

var settings = {
    showGui: true
};

if ( settings.showGui ) {
    var gui = new dat.GUI({width: 320});
    var f1 = gui.addFolder("Controls");
    f1.add( controls, "autoRotate" ).name("Auto Rotate Cam");
    f1.add( controls, "autoRotateSpeed", .1, 16 ).name("Auto Rotate Speed");
    f1.add( controls, "dampingFactor", .01, .5 ).name("Damping Factor");
    f1.add( controls, "enableZoom" ).name("Zoom");
    f1.add( controls, "enablePan" ).name("Pan");
    // f1.open();
}

// --- ANIMATION & RENDER STUFF ----------------------------------------------------

var clock = new THREE.Clock(true);

function interact () {
    controls.update();
}

function moveThings () {
    var t = clock.getElapsedTime();
}

var animate = function() {
    requestAnimationFrame(animate);
    moveThings();
    interact();
    renderer.render(scene,camera);
}

animate();