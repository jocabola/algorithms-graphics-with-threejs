var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setPixelRatio(window.devicePixelRatio||1);

document.querySelector(".container").appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
camera.position.z = 500;
scene.add(camera);

// --- Define Geometry -----------------------
var geo = new THREE.PlaneGeometry(512,512,1,1);

// --- Define Shader Material -------------------

var mat, mesh;

var texture = new THREE.TextureLoader().load("img/UV_Grid.jpg");

ShaderLoader("/glsl/04/shader.vert", "/glsl/04/shader.frag", function (vert, frag) {
    mat = new THREE.ShaderMaterial({
        vertexShader: vert,
        fragmentShader: frag,
        uniforms: {
            map: {value: texture},
            mode: {value: 0}
        }
    });

    // --- Add Mesh to Scene --------
    mesh = new THREE.Mesh(geo, mat);
    scene.add(mesh);
});

// To-Do: show texture mapping on vertex shader!

// ----------------------------------------------

var animate = function() {
    requestAnimationFrame(animate);
    renderer.render(scene,camera);
}

animate();