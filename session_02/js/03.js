var renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.setSize(512,512);
renderer.setPixelRatio(window.devicePixelRatio||1);

document.querySelector(".container").appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-256,256,256,-256, 0.00001, 1000); // left, right, top, bottom
camera.position.z = 500;
scene.add(camera);

// --- Define Geometry -----------------------
var geo = new THREE.PlaneGeometry(400,400,1,1); // -200 to 200 on XY

// add vertex colors per face
let colors = [];
for(let i=0;i<4;i++) {
    let col = new THREE.Color(0xffffff * Math.random());
    colors.push(col);
}

for (let f of geo.faces) {
    f.vertexColors.push(colors[f.a]);
    f.vertexColors.push(colors[f.b]);
    f.vertexColors.push(colors[f.c]);
}

// --- Define Shader Material -------------------

var mat, mesh;

ShaderLoader("/glsl/03/shader.vert", "/glsl/03/shader.frag", function (vert, frag) {
    mat = new THREE.ShaderMaterial({
        vertexShader: vert,
        fragmentShader: frag,
        vertexColors: THREE.VertexColors
    });

    // --- Add Mesh to Scene --------
    mesh = new THREE.Mesh(geo, mat);
    scene.add(mesh);
});

// ----------------------------------------------

var animate = function() {
    requestAnimationFrame(animate);
    renderer.render(scene,camera);
}

animate();